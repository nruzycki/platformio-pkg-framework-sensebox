# Copyright (c) 2014-2015 Arduino LLC.  All right reserved.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

menu.cache=Cache
menu.speed=CPU Speed
menu.opt=Optimize
menu.maxqspi=Max QSPI
menu.usbstack=USB Stack
menu.debug=Debug
menu.power=Power

# senseBox MCU (SAMD21)
sb.name=senseBox MCU
sb.menu.power.on=ON as Default
sb.menu.power.off=OFF as Default
sb.menu.power.off.build.extra_flags=-D__SAMD21G18A__ -DSB_DEFAULT_POWER_OFF {build.usb_flags}
sb.vid.0=0x04D8
sb.pid.0=0xEF66
sb.vid.1=0x04D8
sb.pid.1=0xEF67
sb.bootloader.tool=openocd
sb.bootloader.file=sensebox_mcu.bin
sb.bootloader.size=0x2000
sb.upload.tool=bossac
sb.upload.protocol=sam-ba
sb.upload.maximum_size=262144
sb.upload.use_1200bps_touch=true
sb.upload.wait_for_upload_port=true
sb.upload.native_usb=true
sb.build.mcu=cortex-m0plus
sb.build.f_cpu=48000000L
sb.build.board=SAMD_MKR1000
sb.build.variant=sensebox_mcu
sb.build.core=arduino:arduino
sb.build.extra_flags=-D__SAMD21G18A__ {build.usb_flags}
sb.build.ldscript=linker_scripts/gcc/flash_with_bootloader.ld
sb.build.openocdscript=openocd_scripts/sensebox_mcu.cfg
sb.build.vid=0x04D8
sb.build.pid=0xEF67
sb.build.usb_product="senseBox MCU"
sb.build.usb_manufacturer="senseBox"



